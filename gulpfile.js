var gulp  = require('gulp');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var soften = require('gulp-soften')
var eslint = require('gulp-eslint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var browserify = require('gulp-browserify');
var jasmine = require('gulp-jasmine');
var reporters = require('jasmine-reporters');

var projectFiles = [
	'src/ChessGame.js',
	'src/ChessEngine.js',
	'src/ChessPlayer.js',
	'src/ChessifiedUser.js',
	'src/OnlineChessGame.js',
	'src/ChessGamePOV.js',
	'src/ChessClock.js',
	'src/ChessClockDefault.js'
];
var filesToBrowserify = [
	'src/ChessGame.js'
];


gulp.task('default', ['soften','jasmine','eslint']);



gulp.task('soften', function () {
  gulp.src(projectFiles)
      .pipe(soften(4))
      .pipe(gulp.dest('./src'))
});



gulp.task('jasmine', function() {
	gulp.src('spec/ChessGame-spec')
		.pipe(jasmine({
			'verbose': true,
			'includeStackTrace': true,
			'errorOnFail': true,
			'reporter': new reporters.TerminalReporter()
		}));
});


gulp.task('eslint', function () {
	// ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.
    return gulp.src(['src/ChessGame.js','!node_modules/**'])
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});


/*gulp.task('clean', function() {
	return gulp.src( bases.build ).pipe( clean() );
});
*/
/*gulp.task('scripts', function() {
	// Single entry point to browserify 
	gulp.src('src/js/app.js')
		.pipe(browserify({
		  insertGlobals : true,
		  debug : !gulp.env.production
		}))
		.pipe(gulp.dest('./build/js'))
});*/