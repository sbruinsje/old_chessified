NOTE: not in a working state, currently experimenting...

# About this project #

In this project i will built a new online chess platform similiar to what I did in my previous chess application except this time I will use very different technologies, make use of existing libraries and focus more on design and user interaction.

*The technologies that are or will be used for this project:*

- NodeJS (as backend and for frontend development support)
- jQuery
- Jasmine for testing
- Gulp as build tool
- ESLint for code style checking
- A yet to be chosen frontend (mvc) framework. (Angular or Backbone or React)
- A yet to be chosen database (MySQL or MongoDB) 
