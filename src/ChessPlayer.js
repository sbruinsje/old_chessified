// This object holds all data about a ChessPlayer.
// The recommended PGN fields for player information is described here: http://www.saremba.de/chessgml/standards/pgn/pgn-complete.htm
function ChessPlayer( playerInfo ){
    playerInfo = playerInfo ? playerInfo : {};

    this.firstName =         playerInfo.firstName     ?     playerInfo.firstName         :         "";
    this.middleNames =         playerInfo.middleNames     ?     playerInfo.middleNames         :         "";
    this.lastName =         playerInfo.lastName     ?     playerInfo.lastName         :         "";
    this.title =             playerInfo.title         ?     playerInfo.title             :         ""; // Official FIDE titles
    this.elo =                 playerInfo.elo             ?     playerInfo.elo                 :         "";
    this.type =             playerInfo.type         ?     player.type                 :         ""; // "human" for human players or "program" for computers.
    

}
module.exports = ChessPlayer;

