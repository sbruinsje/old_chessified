'use strict';
var ChessEngine = require('./ChessEngine');
var ChessGamePOV = require('./ChessGamePOV');

function ChessGame(){
    this.chessEngine = new ChessEngine();
    this.pendingDrawOffer = false
    this.drawByAgreement = false;
    this.resigned = false;
    this.lostOnTime = false;
    this.clock = null;
}
module.exports = ChessGame;
ChessGame.prototype.WHITE = 'w';
ChessGame.prototype.BLACK = 'b';
ChessGame.prototype.ENDED_BY_RESIGNATION = 'resignation';
ChessGame.prototype.ENDED_BY_DRAW_AGREEMENT = 'draw_agreement';
ChessGame.prototype.ENDED_BY_PLAYER_CLOCK = 'clock';
ChessGame.prototype.ENDED_BY_CHECKMATE = 'checkmate';
ChessGame.prototype.ENDED_BY_STALEMATE = 'stalemate';
ChessGame.prototype.ENDED_BY_THREEFOLD_REPETITION = 'threefold_repitition';
ChessGame.prototype.ENDED_BY_INSUFFICIENT_MATERIAL = 'insufficient_material';



ChessGame.prototype.getPOV = function(color){
    return new ChessGamePOV(color, this);
};


ChessGame.prototype.loadFen = function(fen){
    this.chessEngine.loadFen(fen);
    return this;    
};
ChessGame.prototype.move = function(move){
    this.pendingDrawOffer = false;
    return this.chessEngine.move(move);
    
};
ChessGame.prototype.isTurnOf = function(color){
    return color ? this.chessEngine.isTurnOf(color) : this.chessEngine.isTurnOf();
};
ChessGame.prototype.inDraw = function(){
    return this.chessEngine.inDraw() || this.isDrawByAgreement();
};
ChessGame.prototype.canOfferDraw = function(color){
    return !this.isTurnOf(color) && !this.hasPendingDrawOffer() && this.hasStarted() && !this.hasEnded();

};
ChessGame.prototype.offerDraw = function(){
    var inactivePlayer = this.isTurnOf() === this.WHITE ? this.BLACK : this.WHITE;
    var canOfferDraw = this.canOfferDraw(inactivePlayer);
    this.pendingDrawOffer = canOfferDraw ? true : false;
    return canOfferDraw;
};
ChessGame.prototype.hasPendingDrawOffer = function(){
    return this.pendingDrawOffer ? true : false;
};
ChessGame.prototype.acceptDrawOffer = function(){
        var canAccept = this.hasPendingDrawOffer();
        this.drawByAgreement = canAccept ? true : this.drawByAgreement;
        this.pendingDrawOffer = false;
        return canAccept;
};
ChessGame.prototype.declineDrawOffer = function(){
    var canDecline = this.hasPendingDrawOffer();
    this.pendingDrawOffer = false;
    return canDecline;
};
ChessGame.prototype.canResign = function(color){
    return !this.hasEnded();
};
ChessGame.prototype.resign = function(color){
    color = color ? color : this.isTurnOf();
    var canResign = this.canResign(color);
    this.resigned = canResign ? color : this.resigned;
    return canResign;
};
ChessGame.prototype.resignedBy = function(color){
    return color ? (this.resigned === color) : this.resigned;
};
ChessGame.prototype.isDrawByAgreement = function(){
    return this.drawByAgreement;
};
ChessGame.prototype.insufficientMaterial = function(color){
    return this.chessEngine.insufficientMaterial();
};
ChessGame.prototype.inThreefoldRepetition = function(){
    return this.chessEngine.inThreefoldRepetition();
};
ChessGame.prototype.fiftyMoveRuleApplies = function(){
    return this.chessEngine.fiftyMoveRuleApplies();
};
ChessGame.prototype.inStalemate = function(){
    return this.chessEngine.inStalemate();    
};
ChessGame.prototype.inCheckmate = function(){
    return this.chessEngine.inCheckmate();    
};
ChessGame.prototype.inCheck = function(){
    return this.chessEngine.inCheck();
};
ChessGame.prototype.hasStarted = function(){
    return this.chessEngine.started() || !!this.resigned;
};
ChessGame.prototype.hasEnded = function(){
    return     !! (this.resigned || this.lostOnTime || this.drawByAgreement || this.chessEngine.gameOver());
};
ChessGame.prototype.lostBy = function(color){
    // if a color is specified return, whether that color has lost.
    if(color){
        return  this.lostOnTime === color ||
                this.resigned === color ||
                this.chessEngine.gameOver() && this.chessEngine.isTurnOf() === color;
    }

    // If no color was specified, then return which player has lost.
    if(this.lostBy(this.WHITE)){
        return this.WHITE;
    } else if(this.lostBy(this.BLACK)){
        return this.BLACK;
    } else{
        return false;
    }

};
ChessGame.prototype.wonBy = function(color){
    var oppositeColor = color === this.WHITE ?  this.BLACK : this.WHITE;

    // if a color is specified return, whether that color has won.
    if(color){
        return  this.lostOnTime === oppositeColor ||
                this.resigned === oppositeColor ||
                this.chessEngine.gameOver() && this.chessEngine.isTurnOf() === oppositeColor;
    }

    // If no color was specified, then return which player has lost.
    if(this.wonBy(this.WHITE)){
        return this.WHITE;
    } else if(this.wonBy(this.BLACK)){
        return this.BLACK;
    } else{
        return false;
    }
};
ChessGame.prototype.gameOverReason = function(){
    if(this.resigned){
        return this.ENDED_BY_RESIGNATION;
    } else if(this.drawByAgreement){
        return this.ENDED_BY_DRAW_AGREEMENT;
    } else if(this.lostOnTime){
        return this.ENDED_BY_PLAYER_CLOCK;
    } else if(this.inCheckmate()){
        return this.ENDED_BY_CHECKMATE;
    } else if(this.inStalemate()){
        return this.ENDED_BY_STALEMATE;
    } else if(this.inThreefoldRepetition()){
        return this.ENDED_BY_THREEFOLD_REPETITION;
    } else if(this.insufficientMaterial()){
        return this.ENDED_BY_INSUFFICIENT_MATERIAL;
    } else{
        return false;
    }
};
ChessGame.prototype.fen = function(){
    return this.chessEngine.fen();
};
ChessGame.prototype.pgn = function(){
    return this.chessEngine.pgn();
};
ChessGame.prototype.ascii = function(){
    return this.chessEngine.ascii();
};
ChessGame.prototype.moveNumber = function(){
    return this.chessEngine.moveNumber();
};
ChessGame.prototype.nrOfHalfMoves = function(){
    return this.chessEngine.nrOfHalfMoves();
};
ChessGame.prototype.getChessEngine = function(){
    return this.chessEngine;
};