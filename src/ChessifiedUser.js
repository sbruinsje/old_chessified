function ChessifiedUser( userData ){
    userData = userData ? userData : {};
    
    this.userId =             userData.userId         ?     userData.userId            :         "";
    this.email =            userData.email            ?     userData.email            :         "";
    this.firstName =         userData.firstName         ?     userData.firstName         :         "";
    this.middleNames =         userData.middleNames     ?     userData.middleNames     :         "";
    this.lastName =         userData.lastName         ?     userData.lastName         :         "";
    this.title =             userData.title             ?     userData.title             :         ""; // Official FIDE titles
    this.elo =                 userData.elo             ?     userData.elo             :         "";
    this.type =             userData.type             ?     userData.type             :         ""; // "human" for human players or "program" for computers.
}
module.exports = ChessifiedUser;

