var ChessGame = require('./ChessGame');

function OnlineChessGame(){
    this.state = null;
    this.gameInfo = null;
    this.chessGame = new ChessGame();
}
module.exports = OnlineChessGame;


OnlineChessGame.prototype.DEFAULT_POLLING_STRATEGY = {
    'pollingStrategy': 'constant',
    'pollingRate': 2000
};



OnlineChessGame.prototype.getPOV = function( color ){
    return new OnlineChessGamePOV( color, this );
};








OnlineChessGame.prototype.newGame = function( options ){
    var startingPosition = options && options.startingPosition ? options.startingPosition : null;
    var whitePlayer = options && options.whitePlayer ? options.whitePlayer : null;
    var blackPlayer = options && options.blackPlayer ? options.blackPlayer : null;
    var joinability = options && options.joinability ? options.joinability : null;
};
OnlineChessGame.prototype.load = function( gameId ){
};
OnlineChessGame.prototype.info = function(){
    // return info about the game: gameid, players, times, etc.
};
OnlineChessGame.prototype.synchronize = function( gameId ){
};
OnlineChessGame.prototype.autoSynchronize = function( options ){
    var pollingStrategy = options && options.pollingStrategy ? options.pollingStrategy : this.POLLING_STRATEGY_CONSTANT;
    var pollingRate = 2000; // in ms
};



OnlineChessGame.prototype.setPlayer = function( color, playerId ){
};
OnlineChessGame.prototype.unsetPlayer = function( color ){
};
OnlineChessGame.prototype.getPlayer = function( color ){
};
OnlineChessGame.prototype.swapPlayers = function(){
};
OnlineChessGame.prototype.move = function(){
};
OnlineChessGame.prototype.turn = function(){
    return this.chessEngine.turn();
};
OnlineChessGame.prototype.lost = function( color ){
};
OnlineChessGame.prototype.won = function( color ){
};
OnlineChessGame.prototype.draw = function(){
};
OnlineChessGame.prototype.drawByAgreement = function(){
};
OnlineChessGame.prototype.insufficientMaterial = function( color ){
};
OnlineChessGame.prototype.threefoldRepetition = function(){
};
OnlineChessGame.prototype.stalemate = function(){
};
OnlineChessGame.prototype.checkmate = function(){
};
OnlineChessGame.prototype.check = function(){
};
OnlineChessGame.prototype.hasStarted = function(){
};
OnlineChessGame.prototype.hasEnded = function(){
};
OnlineChessGame.prototype.canOfferDraw = function(){
};
OnlineChessGame.prototype.offerDraw = function(){
};
OnlineChessGame.prototype.pendingDrawOffer = function(){
};
OnlineChessGame.prototype.acceptDrawOffer = function(){
};
OnlineChessGame.prototype.declineDrawOffer = function(){
};
OnlineChessGame.prototype.canResign = function( color ){
};
OnlineChessGame.prototype.resign = function( color ){
};
OnlineChessGame.prototype.resigned = function(){
};

