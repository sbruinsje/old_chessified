var myApp = angular.module('myApp', []);

myApp.controller('mainController', ['$scope', '$timeout', function($scope, $timeout) {
	$scope.username = 'Stefan';


	$timeout( function(){
		$scope.username = null;
	}, 3000 );


}]);

