function ChessGamePOV( color, chessGame ){
    this.myColor = color ? color : 'white';
    this.chessGame = chessGame;
}
module.exports = ChessGamePOV;

ChessGamePOV.prototype.move = function(){
};
ChessGamePOV.prototype.myColor = function(){
};
ChessGamePOV.prototype.opponentsColor = function(){
};
ChessGamePOV.prototype.myTurn = function(){
    return this.chessGame.turn() === this.myColor;
};
ChessGamePOV.prototype.opponentsTurn = function(){
};
ChessGamePOV.prototype.iLost = function(){
};
ChessGamePOV.prototype.iWon = function(){
};
ChessGamePOV.prototype.drawByAgreement = function(){
};
ChessGamePOV.prototype.result = function(){
    // won, lost, draw
};
ChessGamePOV.prototype.hasStarted = function(){
};
ChessGamePOV.prototype.hasEnded = function(){
};
ChessGamePOV.prototype.canOfferDraw = function(){
};
ChessGamePOV.prototype.offerDraw = function(){
};
ChessGamePOV.prototype.pendingDrawOfferForMe = function(){
};
ChessGamePOV.prototype.pendingDrawOfferForOpponent = function(){
};
ChessGamePOV.prototype.acceptDrawoffer = function(){
};
ChessGamePOV.prototype.declineDrawOffer = function( move ){
    // this is just the same as making a move.
};