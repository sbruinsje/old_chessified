var ChessEngineCore = require('./libs/ChessEngineCore/ChessEngineCore').ChessEngineCore;

function ChessEngine(fen){
    this.chessEngineCore = new ChessEngineCore(fen);
}

module.exports = ChessEngine;

// Load a gamestate based on the given fen string.
ChessEngine.prototype.loadFen = function(fen){
    this.chessEngineCore.load(fen);
    return this;
};

// Loads a gamestate based on the given PGN string (portable game notation)
// pgn: a string containing valid PGN notation
// options: an object containing an optional property "newline_char" that specifies the new-line character that is used.
ChessEngine.prototype.loadPgn = function(pgn, options){
    this.chessEngineCore.load_pgn(pgn, options);
};

// Reset the engine back in the default position (ie. the starting position).
ChessEngine.prototype.reset = function(){
    this.chessEngineCore.reset();
};

// Applies the given move on the board. Where the move is specified as a string or as an object,
// Argument 'move': can be a string in SAN notation or an object containing a 'from' and 'to' property which
//           both specify a square on the board (ie. 'a1' or 'c6'). Íf a promotion is applicable then
//             the move object also has a 'promotion' property that specifies a piece (for instance: ChessEngine.KNIGHT).
// Returns: null if the move failed, or a move object if the move succeeded.
ChessEngine.prototype.move = function(move){
    return this.chessEngineCore.move( move );
};

// Undo the last half move.
// Returns: when succesfull the move object that has been redone or null otherwise.
ChessEngine.prototype.undo = function(){
    return this.chessEngineCore.undo();
};

// Redo the last half move.
// Returns: when succesfull, the move object that has been redone, or null otherwise.
ChessEngine.prototype.redo = function(){
    return this.chessEngineCore.redo();
};

// Clears the board (ie. removes all pieces from it) and set other gamestate accordingly.
ChessEngine.prototype.clear = function(){
    return this.chessEngineCore.clear();
};

// Put a piece on the board.
// Argument piece: an object containing a type property and a color property. Where the type specifies the 
//                 piece (for instance ChessEngine.PAWN) and the color specifies either ChessEngine.BLACK or ChessEngine.WHITE
// Argument square: a string specifying the square to place the piece on (for instance 'a2' or'd5')
// Returns: true when successfull or false when an invalid piece or square was specified.
ChessEngine.prototype.put = function(piece, square){
    return this.chessEngineCore.put(piece, square);
};

// Retrieves the piece on a given square.
// Argument square: the square to get the contents from.
// Results: null if there is no piece on the square or an object with a type and color property that specifies the piece.
ChessEngine.prototype.get = function(square){
    return this.chessEngineCore.get(square);
};


ChessEngine.prototype.winningColor = function(){
    return this.chessEngineCore.winning_color();
};

ChessEngine.prototype.losingColor = function(){
    return this.chessEngineCore.losing_color();
};

// Returns whether the king of the current player is in check.
ChessEngine.prototype.inCheck = function(){
    return this.chessEngineCore.in_check();
};

// Returns whether it is checkmate.
ChessEngine.prototype.inCheckmate = function(){
    return this.chessEngineCore.in_checkmate();
};

// Returns whether it is stalemate.
ChessEngine.prototype.inStalemate = function(){
    return this.chessEngineCore.in_stalemate();
};

// Returns whether the game is a draw.
ChessEngine.prototype.inDraw = function(){
    return this.chessEngineCore.in_draw();
};

// Returns whether there is unsufficient pieces to make checkmate.
ChessEngine.prototype.insufficientMaterial = function(){
    return this.chessEngineCore.insufficient_material();
};

ChessEngine.prototype.fiftyMoveRuleApplies = function(){
    return this.chessEngineCore.fifty_move_rule_applies();
};

// Returns if both players did the same move 3 times in a row.
ChessEngine.prototype.inThreefoldRepetition = function(){
    return this.chessEngineCore.in_threefold_repetition();
};

// Returns whether the game is finished.
ChessEngine.prototype.gameOver = function(){
    return this.chessEngineCore.game_over();
};

// Validates whether a FEN string is valid or not.
// Returns: an object in the form: {valid: true, error_number: 0, error: 'No errors.'}
//            if not valid then the object contains more info, for instance:
//             { valid: false, error_number: 9, error: '1st field (piece positions) is invalid [invalid piece].' }
ChessEngine.prototype.validateFen = function(fen){
    return this.chessEngineCore.validate_fen(fen);
};

// returns the fen string of the current gamestate.
ChessEngine.prototype.fen = function(){
    return this.chessEngineCore.fen();
};

// Returns the Portable Game Notation (PGN) of the current game.
ChessEngine.prototype.pgn = function(){
    return this.chessEngineCore.pgn();
};

// Returns an ascii rerpresentation of the board. This is especially useful in the console/command-line
ChessEngine.prototype.ascii = function(){
    return this.chessEngineCore.ascii();
};

// Returns whoms turn it is if no color was specified (ChessEngine.WHITE or ChessEngine.BLACK)
// or returns true or false if a color was specified.
ChessEngine.prototype.isTurnOf = function(color){
    var turn = this.chessEngineCore.turn();
    if(color){
        return turn === color ? true : false;
    } else{
        return turn;
    }
};

// Returns whether there is a king on the board with the given color.
ChessEngine.prototype.hasKing = function(color){
    return this.chessEngineCore.has_king();
};

// Returns the color of the given square (for instance square 'a2' or b7').
ChessEngine.prototype.squareColor = function(square){
    return this.chessEngineCore.square_color(square);
};

// Returns an array with all the moves in SAN notation.
// Argument options: specifies an object that may contain a 'verbose' property. If this property is set to true 
//                   then the array is not filled with SAN strings but with objects containing various
//                     information about each move.
ChessEngine.prototype.history = function(options){
    return this.chessEngineCore.history(options);
};

// Removes the piece on the given square.
// Returns: null if there was no piece or a piece object if there was (in the form {type: 'q', color: 'w'} )
ChessEngine.prototype.remove = function(square){
    return this.chessEngineCore.remove(square);
};

// Returns an array of all possible moves for the current gamestate.
// Arguments options: an object that may contain one ore more of the following properties:
//         - legal: if set to false only fully legal moves are returned, if not set or set to true psuedo legal moves are also returned.
//        - verbose: Specifies whether the moves should be returned as objects instead of SAN notation.
//        - square: returns the valid moves only for the piece on the given square.
// Returns: an array containing all the moves in SAN format (or an array of objects if verbose was set to true in the options).
ChessEngine.prototype.getMoves = function(options){
    return this.chessEngineCore.moves(options);
};

ChessEngine.prototype.moveNumber = function(){
    return this.chessEngineCore.move_number();
};
ChessEngine.prototype.nrOfHalfMoves = function(){
    return this.chessEngineCore.nr_of_half_moves();
};
ChessEngine.prototype.started = function(){
    return this.chessEngineCore.nr_of_half_moves() > 0;
};