var events = require('events');
var util = require('util');

function ChessClock(){
       events.EventEmitter.call(this); // call the super constructor (EventEmitter) to initialize `this`
    this.lastTickTimestamp = null;
    this.pauseTimestamp = null;
    this.whiteTimeRemainingMs = 15000; // 600000 is 10 minutes
    this.blackTimeRemainingMs = 15000; // 600000 is 10 minutes
    this.activePlayer = this.WHITE;
    this.tickTimeoutHandler = null;
    this.tickIntervalMs = 1000;
}
module.exports = ChessClock;
util.inherits(ChessClock, events.EventEmitter);
ChessClock.prototype.WHITE = 'WHITE';
ChessClock.prototype.BLACK = 'BLACK';
ChessClock.prototype.EVENT_TICK = 'tick';
ChessClock.prototype.EVENT_PRESS = 'press';
ChessClock.prototype.EVENT_START = 'start';
ChessClock.prototype.EVENT_PAUSE = 'pause';
ChessClock.prototype.EVENT_STOP = 'stop';
ChessClock.prototype.EVENT_RESUME = 'resume';
ChessClock.prototype.EVENT_TIME_UP = 'outOfTime';


ChessClock.prototype.press = function(){
    if(!this.lastTickTimestamp){
        return;
    }
    this.processPassedTime();
    this.activePlayer = this.oppositePlayer(this.activePlayer);

    if(this.pressAction){
        this.pressAction();
    }
    
    this.emit(this.EVENT_PRESS);
};

ChessClock.prototype.start = function( activePlayer ){
    if( this.lastTickTimestamp ){
        this.resume();
        return;
    }
    this.activePlayer = activePlayer ? activePlayer : this.activePlayer;
    this.lastTickTimestamp = Date.now();
    this.startTicking();
    this.emit(this.EVENT_START);
};
ChessClock.prototype.pause = function(){
    this.stopTicking();
    this.pauseTimestamp = Date.now();
    this.emit(this.EVENT_PAUSE);
};
ChessClock.prototype.resume = function(){
    if( !this.lastTickTimestamp ){
        this.start();
        return;
    }

    if( this.pauseTimestamp ){
        this.lastTickTimestamp += Date.now() - this.pauseTimeStamp;
    }
    this.startTicking();
    this.emit(this.EVENT_RESUME);
};

ChessClock.prototype.stop = function(){
    this.stopTicking();
    this.lastTickTimestamp = null;
    this.pauseTimestamp = null;
    this.whiteTimeRemainingMs = 0;
    this.blackTimeRemainingMs = 0;
    this.activePlayer = this.WHITE;
    this.emit(this.EVENT_STOP);
};
ChessClock.prototype.tick = function(){
    this.processPassedTime();    
    this.checkRemainingTime(this.activePlayer);
    this.emit(this.EVENT_TICK);
};
ChessClock.prototype.checkRemainingTime = function(player){
    player = player ? player : this.activePlayer;
    var timeRemainingMs = player === this.WHITE ? this.whiteTimeRemainingMs : this.blackTimeRemainingMs;
    if( timeRemainingMs <= 0 ){
        this.emit(this.EVENT_TIME_UP, player);
    }
};




ChessClock.prototype.startTicking = function(){
    this.tickTimeoutHandler = setTimeout( function(){
        this.tick();
        this.lastTickTimestamp = Date.now();
        this.startTicking();
    }.bind(this), this.tickIntervalMs );
};
ChessClock.prototype.stopTicking = function(){
    clearTimeout(this.tickTimeoutHandler);
};
ChessClock.prototype.oppositePlayer = function(color){
    return color === this.WHITE ? this.BLACK : this.WHITE;
};
ChessClock.prototype.processPassedTime = function(){
    var newTimestamp = Date.now();
    var timePassedMs = newTimestamp-this.lastTickTimestamp;
    this.lastTickTimestamp = newTimestamp;

    if(this.activePlayer === this.BLACK){
        this.blackTimeRemainingMs -= timePassedMs;
    } else{
        this.whiteTimeRemainingMs -= timePassedMs;
    }
};

