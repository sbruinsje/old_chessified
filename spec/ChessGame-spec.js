var ChessGame = require("../src/ChessGame");

describe("The ChessGame module", function () {
	var WHITE = ChessGame.prototype.WHITE;
	var BLACK = ChessGame.prototype.BLACK;
	var startingFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
	var fenAfterE4Move = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1";

	beforeEach(function(){
	    this.chessGame = new ChessGame();
	});


	it("can tell whether a player has insufficient material to win", function() {
		// TODO
	});

	it("can apply legal chess moves which updates the gamestate", function() {
		expect(this.chessGame.fen()).toEqual(startingFen);
		expect(this.chessGame.fen()).not.toEqual(fenAfterE4Move);
		var returnValueOfMove = this.chessGame.move({'from': 'e2', 'to': 'e4'});
		expect(typeof returnValueOfMove).toEqual("object");
		expect(returnValueOfMove).not.toEqual(null);
		expect(this.chessGame.fen()).toEqual(fenAfterE4Move);
		expect(this.chessGame.hasStarted()).toBe(true);

	});

	it("returns the correct move/halfmove count when making a legal move", function() {
		expect(this.chessGame.moveNumber()).toBe(1);
		expect(this.chessGame.nrOfHalfMoves()).toBe(0);
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		expect(this.chessGame.moveNumber()).toBe(1);
		expect(this.chessGame.nrOfHalfMoves()).toBe(1);
		this.chessGame.move({'from': 'e7', 'to': 'e5'});
		expect(this.chessGame.moveNumber()).toBe(2);
		expect(this.chessGame.nrOfHalfMoves()).toBe(2);
		this.chessGame.move({'from': 'f2', 'to': 'f4'});
		expect(this.chessGame.moveNumber()).toBe(2);
		expect(this.chessGame.nrOfHalfMoves()).toBe(3);
	});

	it("doesn't apply illegal moves, ie. it doesn't change the gamestate", function() {
		var startPositionFen = this.chessGame.fen();
		var returnValueOfMove = this.chessGame.move({'from': 'e2', 'to': 'e5'});
		var fenAfterIllegalMove = this.chessGame.fen();
		expect(startPositionFen).toEqual(fenAfterIllegalMove);
		expect(startPositionFen).toEqual("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
		expect(returnValueOfMove).toEqual(null);
		expect(this.chessGame.hasStarted()).toBe(false);

	});

	it("should keep track of whose turn it is", function() {
		expect(this.chessGame.isTurnOf()).toEqual(WHITE);
		expect(this.chessGame.isTurnOf(WHITE)).toBe(true);
		this.chessGame.move({'from': 'b1', 'to': 'c3'});
		expect(this.chessGame.isTurnOf()).toEqual(BLACK);
		expect(this.chessGame.isTurnOf(BLACK)).toBe(true);
	});

	it("allows the player whose turn it is to make a draw offer unless there is already a pending drawoffer or the game has not yet been started", function() {
		expect(this.chessGame.canOfferDraw(WHITE)).toBe(false); // game hasnt been started yet.
		expect(this.chessGame.canOfferDraw(BLACK)).toBe(false); 
		var drawOfferSuccessfullyMade = this.chessGame.offerDraw();
		expect(drawOfferSuccessfullyMade).toBe(false);
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		expect(this.chessGame.canOfferDraw(WHITE)).toBe(true);
		expect(this.chessGame.canOfferDraw(BLACK)).toBe(false);
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
		var drawOfferSuccessfullyMade = this.chessGame.offerDraw();
		expect(drawOfferSuccessfullyMade).toBe(true);
		expect(this.chessGame.canOfferDraw(WHITE)).toBe(false);
		expect(this.chessGame.canOfferDraw(BLACK)).toBe(false);
		expect(this.chessGame.hasPendingDrawOffer()).toBe(true);
	});

	it("can check if there is a pending drawoffer", function() {
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		this.chessGame.offerDraw();
		expect(this.chessGame.hasPendingDrawOffer()).toBe(true);
		this.chessGame.declineDrawOffer();
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
		this.chessGame.offerDraw();
		this.chessGame.acceptDrawOffer();
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
	});

	it("allows the player whose move it is to decline a pending draw offer", function() {
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		this.chessGame.offerDraw();
		var declinedSuccessfully = this.chessGame.declineDrawOffer();
		expect(declinedSuccessfully).toBe(true);
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
		var declinedSuccessfully = this.chessGame.declineDrawOffer();
		expect(declinedSuccessfully).toBe(false); // because there is no pending drawoffer now.
		expect(this.chessGame.inDraw()).toBe(false);
	});

	it("allows the player whose move it is to accept a pending draw offer", function() {
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		this.chessGame.offerDraw();
		var acceptedSuccessfully = this.chessGame.acceptDrawOffer();
		expect(acceptedSuccessfully).toBe(true);
		expect(this.chessGame.hasPendingDrawOffer()).toBe(false);
		var acceptedSuccessfully = this.chessGame.acceptDrawOffer();
		expect(acceptedSuccessfully).toBe(false); // because there is no pending drawoffer now
		expect(this.chessGame.inDraw()).toBe(true);
	});

	it("can check if a player can resign", function() {
		expect(this.chessGame.canResign(WHITE)).toBe(true);
		expect(this.chessGame.canResign(BLACK)).toBe(true);
		expect(this.chessGame.canResign()).toBe(true);
		this.chessGame.resign();
		expect(this.chessGame.canResign(WHITE)).toBe(false); // game has ended
		expect(this.chessGame.canResign(BLACK)).toBe(false);
		expect(this.chessGame.canResign()).toBe(false);	
	});

	it("can resign a player if the game has been started and is not yet finished", function() {
		var resignedSuccessfully = this.chessGame.resign(WHITE);
		expect(resignedSuccessfully).toBe(true);
		expect(this.chessGame.hasEnded()).toBe(true);
		expect(this.chessGame.resignedBy()).toBe(WHITE);
		expect(this.chessGame.resignedBy(WHITE)).toBe(true);
		expect(this.chessGame.resignedBy(BLACK)).toBe(false);
		var resignedSuccessfully = this.chessGame.resign(BLACK);
		expect(resignedSuccessfully).toBe(false); // game has ended, white already resigned.
	});

	it("can tell whether the game is currently in checkmate", function() {
		this.chessGame.loadFen("r1bqkbnr/pppp1ppp/2n5/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR w KQkq - 0 1");
		expect(this.chessGame.inCheckmate()).toBe(false);
		this.chessGame.move({'from': 'h5', 'to': 'f7'});
		expect(this.chessGame.inCheckmate()).toBe(true);
	});

	it("can tell whether the game is currently in stalemate", function() {
		this.chessGame.loadFen("8/8/8/8/8/5q2/2k5/4K3 b - - 0 1");
		expect(this.chessGame.inStalemate()).toBe(false);
		this.chessGame.move({'from': 'f3', 'to': 'g2'});
		expect(this.chessGame.inStalemate()).toBe(true);
	});

	it("can tell whether the game has a king in check", function() {
		this.chessGame.loadFen("8/8/8/8/8/5q2/2k5/4K3 b - - 0 1");
		expect(this.chessGame.inCheck()).toBe(false);
		this.chessGame.move({'from': 'f3', 'to': 'e3'});
		expect(this.chessGame.inCheck()).toBe(true);
	});

	it("can tell whether the game is in threefold repetition", function() {
		this.chessGame.loadFen("8/8/8/8/8/5q2/2k5/4K3 b - - 0 1");
		expect(this.chessGame.inThreefoldRepetition()).toBe(false);
		this.chessGame.move({'from': 'f3', 'to': 'e3'});
		this.chessGame.move({'from': 'e1', 'to': 'f1'});
		this.chessGame.move({'from': 'e3', 'to': 'f3'});
		this.chessGame.move({'from': 'f1', 'to': 'e1'});
		this.chessGame.move({'from': 'f3', 'to': 'e3'});
		this.chessGame.move({'from': 'e1', 'to': 'f1'});
		this.chessGame.move({'from': 'e3', 'to': 'f3'});
		this.chessGame.move({'from': 'f1', 'to': 'e1'});
		expect(this.chessGame.inThreefoldRepetition()).toBe(true);
	});

	it("can tell whether the 50 move rule applies so that it becomes a draw", function() {
		expect(this.chessGame.fiftyMoveRuleApplies()).toBe(false);
		for( var i=0; i<25; i++){
			this.chessGame.move({'from': 'b1', 'to': 'c3'});
			this.chessGame.move({'from': 'b8', 'to': 'c6'});
			this.chessGame.move({'from': 'c3', 'to': 'b1'});
			this.chessGame.move({'from': 'c6', 'to': 'b8'});
		}
		expect(this.chessGame.fiftyMoveRuleApplies()).toBe(true);
	});

	it("can tell when the game is in draw", function(){
		var chessGame = new ChessGame();
		expect(chessGame.inDraw()).toBe(false);
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.offerDraw();
		chessGame.acceptDrawOffer();
		expect(chessGame.inDraw()).toBe(true); // draw by agreement
		chessGame = new ChessGame();
		chessGame.loadFen("8/8/8/8/8/1k6/1r6/K7 w - - 0 1");  // stalemate
		expect(chessGame.inDraw()).toBe(true);
		chessGame = new ChessGame();
		chessGame.loadFen("7k/8/8/8/8/8/8/K7 w - - 0 1"); // insufficient material
		expect(chessGame.inDraw()).toBe(true);
		chessGame = new ChessGame();

		// TODO: check if its a draw after fifty moves without progress
		// this is alot of work because the fifty moves cannot be the same 
		// because then the inDraw() function will return true because of the
		// threefold repetition rule.
	});

	it("knows if the game has been started", function() {
		expect(this.chessGame.hasStarted()).toBe(false);
		this.chessGame.move({'from': 'e2', 'to': 'e4'});
		expect(this.chessGame.hasStarted()).toBe(true);
	});

	it("knows if the game has ended", function() {
		var chessGame = new ChessGame();
		expect(chessGame.hasEnded()).toBe(false);
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.resign();
		expect(chessGame.hasEnded()).toBe(true); // resignation
		chessGame = new ChessGame();
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.offerDraw();
		chessGame.acceptDrawOffer();
		expect(chessGame.hasEnded()).toBe(true); // draw by agreement
		chessGame = new ChessGame();
		chessGame.loadFen("8/8/8/8/8/1k6/1r6/K7 w - - 0 1"); // stalemate
		expect(chessGame.hasEnded()).toBe(true);
		chessGame = new ChessGame();
		chessGame.loadFen("7k/8/8/8/8/8/8/K7 w - - 0 1"); // insufficient material
		expect(chessGame.hasEnded()).toBe(true);
		chessGame = new ChessGame();
		chessGame.loadFen("7k/8/8/8/8/2q5/P7/KB6 w - - 0 1"); // checkmate
		expect(chessGame.hasEnded()).toBe(true);
		chessGame = new ChessGame();
		chessGame.loadFen("8/8/8/8/8/5q2/2k5/4K3 b - - 0 1");
		chessGame.move({'from': 'f3', 'to': 'e3'});
		chessGame.move({'from': 'e1', 'to': 'f1'});
		chessGame.move({'from': 'e3', 'to': 'f3'});
		chessGame.move({'from': 'f1', 'to': 'e1'});
		chessGame.move({'from': 'f3', 'to': 'e3'});
		chessGame.move({'from': 'e1', 'to': 'f1'});
		chessGame.move({'from': 'e3', 'to': 'f3'});
		chessGame.move({'from': 'f1', 'to': 'e1'});
		expect(chessGame.hasEnded()).toBe(true); // threefold repetition
	});

	it("can tell who lost (if anyone has)", function() {
		var chessGame = new ChessGame();
		expect(chessGame.lostBy(WHITE)).toBe(false);
		expect(chessGame.lostBy(BLACK)).toBe(false);
		expect(chessGame.lostBy()).toBe(false);
		chessGame.resign();
		expect(chessGame.lostBy(WHITE)).toBe(true);
		expect(chessGame.lostBy(BLACK)).toBe(false);
		expect(chessGame.lostBy()).toBe(WHITE);
		chessGame = new ChessGame();
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.move({'from': 'e7', 'to': 'e5'});
		chessGame.move({'from': 'f1', 'to': 'c4'});
		chessGame.move({'from': 'b8', 'to': 'c6'});
		chessGame.move({'from': 'd1', 'to': 'h5'});
		chessGame.move({'from': 'g8', 'to': 'f6'});
		chessGame.move({'from': 'h5', 'to': 'f7'});
		expect(chessGame.lostBy(WHITE)).toBe(false);
		expect(chessGame.lostBy(BLACK)).toBe(true);
		expect(chessGame.lostBy()).toBe(BLACK);

		// TODO: test if lostBy() gives the right value when a player lost on time.
	});

	it("can tell who won (if anyone has)", function() {
		var chessGame = new ChessGame();
		expect(chessGame.wonBy(WHITE)).toBe(false);
		expect(chessGame.wonBy(BLACK)).toBe(false);
		expect(chessGame.wonBy()).toBe(false);
		chessGame.resign();
		expect(chessGame.wonBy(WHITE)).toBe(false);
		expect(chessGame.wonBy(BLACK)).toBe(true);
		expect(chessGame.wonBy()).toBe(BLACK);
		chessGame = new ChessGame();
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.move({'from': 'e7', 'to': 'e5'});
		chessGame.move({'from': 'f1', 'to': 'c4'});
		chessGame.move({'from': 'b8', 'to': 'c6'});
		chessGame.move({'from': 'd1', 'to': 'h5'});
		chessGame.move({'from': 'g8', 'to': 'f6'});
		chessGame.move({'from': 'h5', 'to': 'f7'});
		expect(chessGame.wonBy(WHITE)).toBe(true);
		expect(chessGame.wonBy(BLACK)).toBe(false);
		expect(chessGame.wonBy()).toBe(WHITE);

		// TODO: test if wonBy() gives the right value when a player won on time.
	});

	it("can tell why the game has ended (if it has)", function() {
		var chessGame = new ChessGame();
		expect(chessGame.gameOverReason()).toBe(false);
		chessGame.resign();
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_RESIGNATION);
		chessGame = new ChessGame();
		chessGame.loadFen("7k/8/8/8/8/8/2q5/K7 w - - 0 1");
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_STALEMATE);
		chessGame = new ChessGame();
		chessGame.move({'from': 'e2', 'to': 'e4'});
		chessGame.offerDraw();
		chessGame.acceptDrawOffer();
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_DRAW_AGREEMENT);
		chessGame = new ChessGame();
		chessGame.loadFen("7k/8/8/8/8/8/8/K7 w - - 0 1");
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_INSUFFICIENT_MATERIAL);
		chessGame = new ChessGame();
		chessGame.loadFen("r1bqkb1r/pppp1Qpp/2n2n2/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 1");
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_CHECKMATE);
		chessGame = new ChessGame();
		chessGame.loadFen("8/8/8/8/8/5q2/2k5/4K3 b - - 0 1");
		chessGame.move({'from': 'f3', 'to': 'e3'});
		chessGame.move({'from': 'e1', 'to': 'f1'});
		chessGame.move({'from': 'e3', 'to': 'f3'});
		chessGame.move({'from': 'f1', 'to': 'e1'});
		chessGame.move({'from': 'f3', 'to': 'e3'});
		chessGame.move({'from': 'e1', 'to': 'f1'});
		chessGame.move({'from': 'e3', 'to': 'f3'});
		chessGame.move({'from': 'f1', 'to': 'e1'});
		expect(chessGame.gameOverReason()).toBe(chessGame.ENDED_BY_THREEFOLD_REPETITION);

		// TODO: test if the gameOverReason() gives the right value when the game ended because of the clock.
	});

	it("can load the game based on a FEN string and return the FEN string of a game", function() {
		var chessGame1 = new ChessGame();
		var chessGame2 = new ChessGame();
		chessGame2.loadFen("r1bqkb1r/pppp1Qpp/2n2n2/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4");
		chessGame1.move({'from': 'e2', 'to': 'e4'});
		chessGame1.move({'from': 'e7', 'to': 'e5'});
		chessGame1.move({'from': 'f1', 'to': 'c4'});
		chessGame1.move({'from': 'b8', 'to': 'c6'});
		chessGame1.move({'from': 'd1', 'to': 'h5'});
		chessGame1.move({'from': 'g8', 'to': 'f6'});
		chessGame1.move({'from': 'h5', 'to': 'f7'});
		expect(chessGame1.fen()).toEqual(chessGame2.fen());
		expect(chessGame1.ascii()).toEqual(chessGame2.ascii());
	});


	it("should start in its starting position by default", function () {
		var startingPositionFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
		expect(this.chessGame.fen()).toEqual(startingPositionFen);
		expect(this.chessGame.isTurnOf()).toEqual(WHITE);
	});
});   